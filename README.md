# Arch basic install

Script de instalación básica de ArchLinux. Es una personalización del script de [Ermanno Ferrari](https://gitlab.com/eflinux/arch-basic)

Pueden revisar su videotutorial "BTRFS & Encryption" (https://www.youtube.com/watch?v=co5V2YmFVEE)
