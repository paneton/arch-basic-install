#!/bin/bash

# Configures the system-wide timezone of the local system
ln -sf /usr/share/zoneinfo/America/Lima /etc/localtime

# Set the Hardware Clock from the System Clock
hwclock --systohc

# Uncomment the en_US.UTF-8 line in /etc/locale.gen
sudo sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen

# locale-gen generates locales for all uncommented locales in /etc/locale.gen
locale-gen

# Setting the system locale
echo "LANG=en_US.UTF-8" >> /etc/locale.conf

# Set keyboard layout to spanish
echo "KEYMAP=es" >> /etc/vconsole.conf

# Set name of the machine
echo "dg3" >> /etc/hostname

# Associates names with IP addresses
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 dg3.localdomain dg3" >> /etc/hosts

# Set root password
echo root:password | chpasswd

# You can add xorg to the installation packages, I usually add it at the DE or WM install script
# You can remove the tlp package if you are installing on a desktop or vm

# Packages installation
pacman -S grub grub-btrfs efibootmgr dhcp dhcpcd networkmanager network-manager-applet iwd dialog wpa_supplicant mtools dosfstools base-devel linux-headers xdg-user-dirs xdg-utils bluez cups alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack openssh rsync reflector acpi acpi_call tlp flatpak acpid os-prober ntfs-3g neofetch

# pacman -S --noconfirm xf86-video-amdgpu
pacman -S --noconfirm nvidia nvidia-utils nvidia-settings

grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB #change the directory to /boot/efi if you mounted the EFI partition at /boot/efi

grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable dhcpcd
systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd
systemctl enable tlp # You can comment this command out if you didn't install tlp, see above
systemctl enable reflector.timer
systemctl enable fstrim.timer #info at https://www.digitalocean.com/community/tutorials/how-to-configure-periodic-trim-for-ssd-storage-on-linux-servers
systemctl enable acpid

useradd -m torquemada
echo torquemada:password | chpasswd

echo "torquemada ALL=(ALL) ALL" >> /etc/sudoers.d/torquemada

printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"
